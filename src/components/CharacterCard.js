import React from "react"
import { makeStyles } from "@mui/styles"
import Button from "@mui/material/Button"
import Card from "@mui/material/Card"
import CardActions from "@mui/material/CardActions"
import CardContent from "@mui/material/CardContent"
import Typography from "@mui/material/Typography"

const useStyles = makeStyles({
  characterCard: {
    minHeight: 300,
    transition: "transform 0.3s ease",
    "&:hover": {
      transform: "scale(1.05)",
    },
    "& .imgCard": {
      width: "100%",
      objectFit: "cover",
    },
  },
  buttonContainer: {
    display: "flex",
    justifyContent: "flex-end",
  },
})

const CharacterCard = ({
  character,
  handleCharacterSelect,
  isSelected,
  details = false,
}) => {
  const classes = useStyles()

  const handleClick = () => {
    handleCharacterSelect(character)
  }

  return (
    <Card className={classes.characterCard}>
      <img src={character.image} alt={character.name} className="imgCard" />
      <CardContent style={{ display: "flex", flexDirection: "column", padding: "0 10px" }}>
        {details && (
          <Typography variant="h5" component="h3">
            {character.name}
          </Typography>
        )}
        {details && (
          <Typography variant="body2" color="textSecondary">
            Status: {character.status}
          </Typography>
        )}
        {details && (
          <Typography variant="body2" color="textSecondary">
            Specie: {character.species}
          </Typography>
        )}
        {details && (
          <Typography variant="body2" color="textSecondary">
            Origin: {character.origin.name}
          </Typography>
        )}
        {!details && (
          <CardActions className={classes.buttonContainer}>
            <Typography variant="h5" component="h3">
              {character.name}
            </Typography>
            <Button onClick={handleClick}>
              {isSelected ? (
                <img height={50} src="/select.svg" alt="select" />
              ) : (
                <img height={50} src="/deselect.svg" alt="deselect" />
              )}
            </Button>
          </CardActions>
        )}
      </CardContent>
    </Card>
  )
}

export default CharacterCard
