import React, { useState, useEffect } from "react"
import Snackbar from "@mui/material/Snackbar"
import MuiAlert from "@mui/material/Alert"

const Notification = ({ message }) => {
  const [queue, setQueue] = useState([])
  const [open, setOpen] = useState(false)

  useEffect(() => {
    if (queue.length > 0 && !open) {
      setOpen(true)
    }
  }, [queue, open])

  const handleClose = () => {
    setOpen(false)
    setQueue((prevQueue) => prevQueue.slice(1))
  }

  const enqueueMessage = (newMessage) => {
    setQueue((prevQueue) => [...prevQueue, newMessage])
  }

  useEffect(() => {
    if (message) {
      enqueueMessage(message)
    }
  }, [message])

  return (
    <Snackbar
      open={open}
      autoHideDuration={3000}
      onClose={handleClose}
      anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
    >
      <MuiAlert
        elevation={6}
        variant="filled"
        severity="error"
        onClose={handleClose}
      >
        {queue[0]}
      </MuiAlert>
    </Snackbar>
  )
}

export default Notification
