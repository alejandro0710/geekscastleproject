import React from "react"
import { Link } from "react-router-dom"
import List from "@mui/material/List"
import ListItem from "@mui/material/ListItem"
import ListItemText from "@mui/material/ListItemText"
import Divider from "@mui/material/Divider"
import Typography from "@mui/material/Typography"

const Sidebar = () => {
  return (
    <div className="sidebar">
      <Typography variant="h5" gutterBottom>
        Menu
      </Typography>
      <List component="nav">
        <ListItem button component={Link} to="/">
          <ListItemText primary="List of Characters" />
        </ListItem>
        <Divider />
        <ListItem button component={Link} to="/detail/1">
          <ListItemText primary="Character Details" />
        </ListItem>
      </List>
    </div>
  )
}

export default Sidebar
