import React from "react"
import Grid from "@mui/material/Grid"
import CharacterCard from "./CharacterCard"

const CharacterDetail = ({ character }) => {
  return (
    <Grid container spacing={2}>
      {character.map((data) => (
        <Grid key={data.id} item xs={12} sm={6} md={4} lg={3}>
          <CharacterCard character={data} details={true} />
        </Grid>
      ))}
    </Grid>
  )
}

export default CharacterDetail
