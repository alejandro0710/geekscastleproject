import React, { useEffect, useState } from "react"
import { useSelector, useDispatch } from "react-redux"
import {
  fetchCharacters,
  selectCharacter,
  deselectCharacter,
} from "../store/actions/characterActions"
import CharacterCard from "./CharacterCard"
import Grid from "@mui/material/Grid"
import Typography from "@mui/material/Typography"
import Notification from "./Notification"
import Loading from "./Loading"
import TextField from "@mui/material/TextField"
import { makeStyles } from "@mui/styles"

const useStyles = makeStyles((theme) => ({
  root: {
    paddingTop: 30,
  },
  searchInput: {
    marginBottom: 30,
  },
}))

const CharacterList = () => {
  const dispatch = useDispatch()
  const { characters, loading, error, selectedCharacters } = useSelector(
    (state) => state
  )
  const [notification, setNotification] = useState("")
  const [searchTerm, setSearchTerm] = useState("")
  const classes = useStyles()

  useEffect(() => {
    dispatch(fetchCharacters())
  }, [dispatch])

  const handleCharacterSelect = (character) => {
    if (selectedCharacters.some((char) => char.id === character.id)) {
      dispatch(deselectCharacter(character))
    } else {
      if (selectedCharacters.length >= 3 && !character.isSelected) {
        setNotification("Only up to three items can be selected from the list")
        setTimeout(() => {
          setNotification(false)
        }, 1800)
        return
      }
      dispatch(selectCharacter(character))
    }
  }

  const filterCharactersByName = () => {
    return characters.filter((character) =>
      character.name.toLowerCase().includes(searchTerm.toLowerCase())
    )
  }

  if (loading) {
    return <Loading />
  }

  if (error) {
    return (
      <div style={{ textAlign: "center", padding: "20px" }}>
        <Typography variant="body1" color="error">
          Error: {error}
        </Typography>
      </div>
    )
  }

  return (
    <div className={classes.root}>
      <Notification message={notification} />
      <TextField
        className={classes.searchInput}
        label="Search by name"
        variant="outlined"
        value={searchTerm}
        onChange={(e) => setSearchTerm(e.target.value)}
      />
      <Grid style={{ marginTop: 20 }} container spacing={2}>
        {filterCharactersByName().map((character) => (
          <Grid key={character.id} item xs={12} sm={6} md={4} lg={3}>
            <CharacterCard
              character={character}
              handleCharacterSelect={handleCharacterSelect}
              isSelected={selectedCharacters.some((c) => c.id === character.id)}
            />
          </Grid>
        ))}
      </Grid>
    </div>
  )
}

export default CharacterList
