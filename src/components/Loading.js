import React from "react"

const Loading = () => {
  return (
    <div style={{ textAlign: "center", padding: "20px" }}>
      <img
        src="/loading.webp"
        alt="Loading..."
        style={{
          width: "200px",
          animation: "spin 5s linear infinite",
        }}
      />
      <h3>LOADING ...</h3>
      <style>
        {`
          @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
          }
        `}
      </style>
    </div>
  )
}

export default Loading
