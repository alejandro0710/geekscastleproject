import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { IconButton, Typography } from "@mui/material";
import { setTheme } from "../store/actions/characterActions";
import { makeStyles } from "@mui/styles";

const useStyles = makeStyles((theme) => ({
  header: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    padding: 20,
    background: props => props.theme.primary,
    color: theme.text,
  },
  logo: {
    width: "100px",
    marginRight: "auto",
    filter: (props) => (props.darkMode ? "invert(1)" : "invert(0)"),
  },
}));

const Header = () => {
  const darkMode = useSelector((state) => state.darkMode);
  const theme = useSelector((state) => state.theme);
  const dispatch = useDispatch();
  const classes = useStyles({ theme, darkMode });

  const handleThemeChange = () => {
    const newThemeName = darkMode ? "light" : "dark";
    dispatch(setTheme(newThemeName));
  };

  return (
    <header className={classes.header}>
      <picture>
        <img src={"/logo.png"} alt="Logo" className={classes.logo} />
      </picture>
      <Typography variant="h6">Welcome to my Rick and Morty app</Typography>
      <IconButton onClick={handleThemeChange} color="red">
        <img src="/darkmode.png" alt="darkmode" />
      </IconButton>
    </header>
  );
};

export default Header;
