import { lightTheme, darkTheme } from "../../themes/Themes.js"
import {
  SET_THEME,
  FETCH_CHARACTERS_REQUEST,
  FETCH_CHARACTERS_SUCCESS,
  FETCH_CHARACTERS_FAILURE,
  SELECT_CHARACTER,
  DESELECT_CHARACTER,
  CLEAR_SELECTED_CHARACTERS,
  SHOW_NOTIFICATION,
  FETCH_CHARACTER_BY_ID_REQUEST,
  FETCH_CHARACTER_BY_ID_SUCCESS,
  // FETCH_CHARACTER_BY_ID_FAILURE
} from "../actions/characterActions.js"

const initialState = {
  characters: [],
  selectedCharacters: [],
  loading: false,
  error: "",
  darkMode: false,
  theme: lightTheme,
}

const characterReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_THEME:
      const newTheme = action.payload === "dark" ? darkTheme : lightTheme
      return {
        ...state,
        darkMode: action.payload === "dark",
        theme: newTheme,
      }
    case FETCH_CHARACTERS_REQUEST:
      return {
        ...state,
        loading: true,
        error: "",
      }
    case FETCH_CHARACTERS_SUCCESS:
      return {
        ...state,
        loading: false,
        characters: action.payload,
        error: "",
      }
    case FETCH_CHARACTERS_FAILURE:
      return {
        ...state,
        loading: false,
        characters: [],
        error: action.payload,
      }
    case FETCH_CHARACTER_BY_ID_REQUEST:
      return {
        ...state,
        loading: true,
        error: "",
      }
    case FETCH_CHARACTER_BY_ID_SUCCESS:
      return {
        ...state,
        loading: false,
        selectedCharacter: action.payload,
        error: "",
      }
    case SELECT_CHARACTER:
      return {
        ...state,
        selectedCharacters: [...state.selectedCharacters, action.payload],
      }
    case DESELECT_CHARACTER:
      return {
        ...state,
        selectedCharacters: state.selectedCharacters.filter(
          (character) => character.id !== action.payload.id
        ),
      }
    case SHOW_NOTIFICATION:
      return {
        ...state,
        notification: action.payload,
      }
    case CLEAR_SELECTED_CHARACTERS:
      return {
        ...state,
        selectedCharacters: [],
      }
    default:
      return state
  }
}

export default characterReducer
