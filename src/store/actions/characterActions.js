import axios from 'axios';

export const FETCH_CHARACTERS_REQUEST = 'FETCH_CHARACTERS_REQUEST';
export const FETCH_CHARACTERS_SUCCESS = 'FETCH_CHARACTERS_SUCCESS';
export const FETCH_CHARACTERS_FAILURE = 'FETCH_CHARACTERS_FAILURE';

export const fetchCharacters = () => {
  return async (dispatch) => {
    dispatch({ type: FETCH_CHARACTERS_REQUEST });
    try {
      const response = await axios.get('https://rickandmortyapi.com/api/character');
      console.log("Aqui optengo todos los datos del API: ",response.data.results);
      dispatch({ type: FETCH_CHARACTERS_SUCCESS, payload: response.data.results });
    } catch (error) {
      dispatch({ type: FETCH_CHARACTERS_FAILURE, payload: error.message });
    }
  };
};

export const SET_THEME = 'SET_THEME';

export const setTheme = (themeName) => ({
  type: SET_THEME,
  payload: themeName,
});

export const FETCH_CHARACTER_BY_ID_REQUEST = 'FETCH_CHARACTER_BY_ID_REQUEST';
export const FETCH_CHARACTER_BY_ID_SUCCESS = 'FETCH_CHARACTER_BY_ID_SUCCESS';
export const FETCH_CHARACTER_BY_ID_FAILURE = 'FETCH_CHARACTER_BY_ID_FAILURE';

export const fetchCharacterById = (id) => {
  return async (dispatch) => {
    dispatch({ type: FETCH_CHARACTER_BY_ID_REQUEST });
    try {
      const response = await axios.get(`https://rickandmortyapi.com/api/character/${id}`);
      dispatch({ type: FETCH_CHARACTER_BY_ID_SUCCESS, payload: response.data });
    } catch (error) {
      dispatch({ type: FETCH_CHARACTER_BY_ID_FAILURE, payload: error.message });
    }
  };
};

export const SELECT_CHARACTER = 'SELECT_CHARACTER';

export const selectCharacter = (character) => {
  return (dispatch, getState) => {
    const { selectedCharacters } = getState();
    
    if (selectedCharacters.length >= 3 && !selectedCharacters.find(char => char.id === character.id)) {
      dispatch(showNotification("Only up to three items can be selected from the list"));
      return;
    }
    
    dispatch({
      type: SELECT_CHARACTER,
      payload: character
    });
  };
};

export const SHOW_NOTIFICATION = 'SHOW_NOTIFICATION';

export const showNotification = (message) => {
  return {
    type: SHOW_NOTIFICATION,
    payload: message
  };
};

export const DESELECT_CHARACTER = 'DESELECT_CHARACTER';

export const deselectCharacter = (character) => {
  return {
    type: DESELECT_CHARACTER,
    payload: character
  };
};

export const CLEAR_SELECTED_CHARACTERS = 'CLEAR_SELECTED_CHARACTERS';


export const clearSelectedCharacters = () => ({
  type: CLEAR_SELECTED_CHARACTERS,
});
