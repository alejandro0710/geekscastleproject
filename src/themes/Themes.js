export const lightTheme = {
  text: "#171e07",
  background: "#fbfdf6",
  primary: "#a6d132",
  secondary: "#85e497",
  accent: "#6cdea5",
}

export const darkTheme = {
  text: "#f2f8e2",
  background: "#070802",
  primary: "#a2cc2e",
  secondary: "#1b792c",
  accent: "#219159",
}
