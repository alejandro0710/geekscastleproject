// screens/DetailScreen.js

import React from "react"
import CharacterDetail from "../components/CharacterDetail"
import { useSelector, useDispatch } from "react-redux"
import Button from "@mui/material/Button"
import Notification from "../components/Notification"
import { clearSelectedCharacters } from "../store/actions/characterActions"
import { makeStyles } from "@mui/styles"

const useStyles = makeStyles((theme) => ({
  detailScreen: {
    backgroundColor: theme.background,
    color: theme.text,
    minHeight: "100vh",
  },
  contentButton: {
    display: "flex",
    justifyContent: "flex-end",
    marginBottom: 20,
  },
  clearButton: {
    backgroundColor: theme.secondary,
    color: theme.text,
    border: "none",
    cursor: "pointer",
    "&:hover": {
      backgroundColor: theme.secondary,
    },
  },
}))

const DetailScreen = () => {
  const selectedCharacters = useSelector((state) => state.selectedCharacters)
  const dispatch = useDispatch()
  const classes = useStyles()

  const handleClearSelectedCharacters = () => {
    dispatch(clearSelectedCharacters())
  }

  return (
    <div className={classes.detailScreen}>
      {selectedCharacters.length ? (
        <div>
          <div className={classes.contentButton}>
            <Button
              variant="contained"
              className={classes.clearButton}
              onClick={handleClearSelectedCharacters}
            >
              Clear selected
            </Button>
          </div>
          <CharacterDetail character={selectedCharacters} />
        </div>
      ) : (
        <Notification message="No character selected" />
      )}
    </div>
  )
}

export default DetailScreen
