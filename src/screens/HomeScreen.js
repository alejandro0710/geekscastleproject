import React from 'react';
import { useSelector } from "react-redux";
import { useNavigate } from 'react-router-dom';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import { makeStyles } from '@mui/styles';
import CharacterList from '../components/CharacterList';

const useStyles = makeStyles((theme) => ({
  titleContainer: {
    display: 'flex',
    justifyContent: 'space-between',
  },
}));

const HomeScreen = ({ characters }) => {
  const theme = useSelector((state) => state.theme);
  const classes = useStyles({theme});
  const navigate = useNavigate();
  
  const handleViewDetails = () => {
    navigate('/detail/1');
  };

  return (
    <div>
      <Typography className={classes.titleContainer} variant="h4" gutterBottom>
        List of Characters
        <Button variant="contained" onClick={handleViewDetails}>
          View Details
        </Button>
      </Typography>
      <CharacterList characters={characters} />
    </div>
  );
}

export default HomeScreen;
