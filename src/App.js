import React from "react"
import { BrowserRouter as Router, Routes, Route } from "react-router-dom"
import { ThemeProvider, createTheme } from "@mui/material/styles"
import HomeScreen from "./screens/HomeScreen"
import DetailScreen from "./screens/DetailScreen"
import Sidebar from "./components/Sidebar"
import Grid from "@mui/material/Grid"
import Box from "@mui/material/Box"
import Container from "@mui/material/Container"
import { lightTheme, darkTheme } from "./themes/Themes"
import Header from "./components/Header"

const lightMuiTheme = createTheme({
  palette: {
    primary: {
      main: lightTheme.primary,
    },
    secondary: {
      main: lightTheme.secondary,
    },
    text: {
      primary: lightTheme.text,
    },
    background: {
      default: lightTheme.background,
    },
  },
})

// const darkMuiTheme = createTheme({
//   palette: {
//     primary: {
//       main: darkTheme.primary,
//     },
//     secondary: {
//       main: darkTheme.secondary,
//     },
//     text: {
//       primary: darkTheme.text,
//     },
//     background: {
//       default: darkTheme.background,
//     },
//   },
// })

function App() {
  // const darkMode = useSelector((state) => state.darkmode)

  return (
    <ThemeProvider theme={lightMuiTheme}>
      <Box minHeight="100vh" display="flex" flexDirection="column">
        <Header />
        <Router>
          <Container maxWidth="lg" style={{ flexGrow: 1 }}>
            <Box mt={4} mb={4}>
              <Grid container spacing={3}>
                <Grid item xs={12} md={3}>
                  <Sidebar />
                </Grid>
                <Grid item xs={12} md={9}>
                  <div className="content">
                    <Routes>
                      <Route path="/" element={<HomeScreen />} />
                      <Route path="/detail/:id" element={<DetailScreen />} />
                    </Routes>
                  </div>
                </Grid>
              </Grid>
            </Box>
          </Container>
        </Router>
      </Box>
    </ThemeProvider>
  )
}

export default App
